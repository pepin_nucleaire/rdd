# [Trello](https://trello.com/b/wzGHzhLW/rdd-site)


# Apprentissage du devweb au sigmalien débutant

## Logiciel à utiliser
- [Visual Studio Code : Version libre de visual studio super pratique](https://code.visualstudio.com/)
- Installer les extensions ```python```, et autres que je vais lister

## LA BASE
- [HTML / CSS (mise en page)](https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/1604192-comment-fait-on-pour-creer-des-sites-web)
- [Python (Logique du site)](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230659-quest-ce-que-python)
- [Git (outil pour pouvoir versionner la source)](https://tutorialzine.com/2016/06/learn-git-in-30-minutes)

## FRAMEWORK CSS POUR FACILITER LA MISE EN PAGE
- [Apprendre Bootstrap](https://medium.freecodecamp.org/learn-bootstrap-4-in-30-minute-by-building-a-landing-page-website-guide-for-beginners-f64e03833f33)
- [Utilisation des composants bootstrap](https://bootstrap.build/app/v4.1.1/)
- [Pingendo : Générateur de mise en page "Facile" en bootstrap](https://v40.pingendo.com/new)

## Framework web Python (Flask) pour la logique
- [MegaTuto par Miguel Grinberg (c'est une mine d'or)](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world)
- [Ressources flask (comment j'ai essayé de suivre l'architecture du site)](https://exploreflask.com/en/latest/)
- [CRUD App (autres ressources que j'ai pu utilisé)](https://scotch.io/tutorials/build-a-crud-web-app-with-python-and-flask-part-one)

# Workflow à avoir pour adapter le site à un gala / RDD

- Comprendre comment le site marche (Demander à julien muller si soucis)
- Installer l'environnement de test sur son PC ```python -> pip -> virtualenv -> flask -> requirements -> variables d'environnement``` (Faire guide précis windobe)
- Adapter le fichier ```config.env``` à son site
- Pour commencer à travailler, établir la mise en forme du site = Faire un dessin de ce que l'on veut
- Utiliser [pingendo](https://v40.pingendo.com/new) pour obtenir son site de manière visuelle 
- Adapter les fichiers exportés pour qu'ils marchent pour le site (notamment sur les parties anonyme/connecté + privé)

À compléter avec l'expérience


# Installation du serveur de développement sur son PC

## Linux (peut-être windows si installé sur le PATH?)

1. Fork le repo et clone 
1. Installer [```python3```](https://www.python.org/) et [```pip3```](https://pip.pypa.io/en/latest/installing/)
2. Installer [```virtualenvwrapper```](https://python-guide-pt-br.readthedocs.io/fr/latest/dev/virtualenvs.html) avec la commande ```pip3 install virtualenvwrapper```
3. ```mkvirtualenv flask```
4. ```workon flask```
5. ```pip install -r requirements.txt```
6. Pour lancer le serveur localement ```export FLASK_APP=run.py``` puis ```flask run```


# TODO

- modifier le config.example.env en config.env et l'adapter
- Pour mettre en local, ne pas oublier ```export FLASK_APP=run.py``` & ```export FLASK_DEBUG=1```

