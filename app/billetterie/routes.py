from flask import render_template, redirect, request, url_for, flash, Blueprint, current_app
from flask_login import  login_required, current_user
from .. import db
from ..models import User, DiplomeesSigma

bp = Blueprint('billetterie', __name__, template_folder='templates')

@bp.route('/billetterie', methods=['GET','POST'])
@login_required
def billetterie():
    return render_template("billetterie.html", title="Billeterie")
    
