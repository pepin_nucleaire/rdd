from flask import render_template, redirect, request, url_for, flash, Blueprint, current_app, Response, jsonify
from flask_login import login_user, logout_user, login_required, current_user
from .. import db
from ..models import User, Sigmemories, DiplomeesSigma,PlanDeTable
import csv
import json
# Preinscription
from random import choice
import string
from datetime import datetime

bp = Blueprint('admin', __name__, template_folder='templates',
               url_prefix="/admin")


def check_admin():
    user = current_user
    if user.admin:
        pass
    else:
        flash("You're not an admin.")
        return redirect(url_for('main.index'))


@bp.route('/', methods=['GET'])
@login_required
def admin():
    check_admin()
    return render_template('admin.html', title='Administration')

def get_all_sigmemories():
    memories = Sigmemories.query.all()
    return memories

def get_plan():
    plan_export = []
    plan = PlanDeTable.query.all()
    return plan


@bp.route('/api-plan')
def api_plan():
    plan = [ table.serialize for table in get_plan() ]
    return jsonify(plan)

@bp.route('/api-plan-uniq')
def api_plan_uniq():
    plan = get_plan()
    liste=[]
    for table in plan:
        for place in [table.place1,table.place2,table.place3,table.place4,table.place5,table.place6,table.place7,table.place8,table.place9,table.place10]:
            if place is not None:
                user = User.query.filter_by(id=place).first()
                if user.serialize not in liste:
                    liste.append(user.serialize)
    return jsonify(liste)


@bp.route('/gensigmortel')
def gensigmortel():
    check_admin()
    memories = [ memory.serialize for memory in get_all_sigmemories() ]
    write_csv(memories)
    with open("sigmemories_output/sigmemories.csv") as fp:
        csv = fp.read()
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=sigmemories.csv"})

@bp.route('/api-gensigmortel')
def api_gensigmortel():
    memories = [ memory.serialize for memory in get_all_sigmemories() ]
    return jsonify(memories)

def write_csv(data):
    with open('sigmemories_output/sigmemories.csv', 'w') as f:
        w=csv.DictWriter(f, data[0].keys())
        w.writeheader()
        w.writerows(data)

@bp.route('/sigmortel')
def sigmortel():
    check_admin()
    memories = get_all_sigmemories()
    return render_template('sigmortel.html', memories=memories)

@bp.route('/liste')
def liste():
    check_admin()
    liste = User.query.filter_by(diplome=True, email_confirmed=False).all()
    diplomes = DiplomeesSigma.query.all()
    count_diplomes_non_inscrits = str(len(liste)) + " / "+str(len(diplomes))
    return render_template('liste.html', liste=liste, restant=count_diplomes_non_inscrits)


@bp.route('/preinscription')
def preinscription():
    check_admin()
    diplomes = DiplomeesSigma.query.all()
    for diplome in diplomes:
        pw = "".join(choice(string.ascii_lowercase) for i in range(12))
        new_user = User(diplome.email, pw, diplome.prenom, diplome.nom)
        new_user.email_confirmation_sent_on = datetime.now()
        new_user.diplome = True
        db.session.add(new_user)
        send_confirmation(new_user.email, pw)
    db.session.commit()
    flash('Preinscription faite')
    return redirect(url_for('admin.admin'))


from itsdangerous import Serializer, URLSafeTimedSerializer
from ..email import send_email


def send_confirmation(user_email, pw):
    confirm_serializer = URLSafeTimedSerializer(
        current_app.config['SECRET_KEY'])

    confirm_url = url_for(
        'auth.confirm_email',
        token=confirm_serializer.dumps(
            user_email, salt='email-confirmation-salt'),
        _external=True)

    html = render_template(
        'email_confirmation_dipl.html',
        confirm_url=confirm_url, pw=pw)

    send_email('Confirm Your Email Address', [user_email], html)
