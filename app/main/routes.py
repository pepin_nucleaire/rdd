from flask import render_template, Blueprint

bp = Blueprint('main', __name__, template_folder='templates')


@bp.route('/')
@bp.route('/index')
def index():
    return render_template('index.html', title='Home', main_page=True)
    
@bp.route('/mentions')
def mentions():
    return render_template('mentions.html', title='Mentions Légales', main_page=False)
