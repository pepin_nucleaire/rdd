from flask import render_template, redirect, request, url_for, flash, Blueprint, current_app
from flask_login import login_user, logout_user, login_required, current_user
from .. import db
from ..models import User, Sigmemories, DiplomeesSigma
from .forms import Dedicace, ModDedicace

bp = Blueprint('private', __name__, template_folder='templates')


@bp.route('/newdedi', methods=['GET', 'POST'])
@login_required
def new_dedi():
    form = Dedicace(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            if form.diplomee.data:
                dedi = Sigmemories(diplome=form.diplomee.data.id)
                dedi.ipq1 = form.ipq1.data
                dedi.ipq2 = form.ipq2.data
                dedi.ipq3 = form.ipq3.data
                dedi.surnom1 = form.surnom1.data
                dedi.surnom2 = form.surnom2.data
                dedi.surnom3 = form.surnom3.data
                dedi.expression = form.expression.data
                dedi.inbattable = form.imbattable.data
                dedi.souvenir = form.souvenir.data
                dedi.deviendra = form.deviendra.data
                dedi.dedicace = form.dedicace.data
                dedi.id_auteur = current_user.get_id()
                db.session.add(dedi)
                db.session.commit()
                flash('Tag ajouté !', 'success')
                return redirect(url_for('private.listdedi'))
            else:
                flash('Veuillez sélectionner un diplômé', 'error')
    return render_template("dedicasse.html", form=form, title='Sigmemories')


def query_diplomee():
    diplos = DiplomeesSigma.query.all()
    tuple_list = []
    for diplo in diplos:
        element = (diplo.id, diplo)
        tuple_list.append(element)
    return tuple_list


@bp.route('/moddedi/<id>', methods=['GET', 'POST'])
@login_required
def moddedi(id):
    dedi = Sigmemories.query.filter_by(id=id).first()
    diplo = DiplomeesSigma.query.filter_by(id=dedi.id_diplome).first()
    nom_diplo = diplo
    form = ModDedicace(request.form)
    if request.method == 'GET':
        form.ipq1.data = dedi.ipq1
        form.ipq2.data = dedi.ipq2
        form.ipq3.data = dedi.ipq3
        form.surnom1.data = dedi.surnom1
        form.surnom2.data = dedi.surnom2
        form.surnom3.data = dedi.surnom3
        form.expression.data = dedi.expression
        form.imbattable.data = dedi.inbattable
        form.souvenir.data = dedi.souvenir
        form.deviendra.data = dedi.deviendra
        form.dedicace.data = dedi.dedicace
    if request.method == 'POST':
        if form.validate_on_submit():
                dedi.ipq1 = form.ipq1.data
                dedi.ipq2 = form.ipq2.data
                dedi.ipq3 = form.ipq3.data
                dedi.surnom1 = form.surnom1.data
                dedi.surnom2 = form.surnom2.data
                dedi.surnom3 = form.surnom3.data
                dedi.expression = form.expression.data
                dedi.inbattable = form.imbattable.data
                dedi.souvenir = form.souvenir.data
                dedi.deviendra = form.deviendra.data
                dedi.dedicace = form.dedicace.data
                dedi.id_auteur = current_user.get_id()
                db.session.add(dedi)
                db.session.commit()
                print('ok')
                flash('Tag édité !', 'success')
                return redirect(url_for('private.listdedi'))
    return render_template("moddedi.html", form=form, title='Sigmemories', diplo=diplo)


@bp.route('/deldedi/', methods=['GET', 'POST'])
@login_required
def deldedi():
    id = request.form['id'] # To get id provided by JS 
    dedi = Sigmemories.query.get_or_404(id)
    db.session.delete(dedi)
    db.session.commit()
    flash('Suppresion du tag faite')
    return redirect(url_for('private.listdedi'))


@bp.route('/sigmemories')
@login_required
def listdedi():
    dedicaces_faites = Sigmemories.query.filter_by(
        id_auteur=current_user.get_id()).all()
    dedi_diplo = dict()
    for dedi in dedicaces_faites:
        diplo = DiplomeesSigma.query.filter_by(
            id=dedi.id_diplome).first()
        dedi_diplo[dedi] = diplo
    return render_template('sigmemories.html', dedi_diplo=dedi_diplo)
