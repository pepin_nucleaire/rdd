from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField, ValidationError, TextField, TextAreaField, SelectField
from wtforms.validators import DataRequired, Email, Length, EqualTo
from ..models import User, Sigmemories, DiplomeesSigma
from wtforms_sqlalchemy.fields import QuerySelectField


class Dedicace(FlaskForm):
    diplomee = QuerySelectField('Diplomé(e)', blank_text='Sélectionnez le futur diplômé', allow_blank=True,
                                query_factory=lambda: DiplomeesSigma.query.order_by(DiplomeesSigma.nom).all() )
    surnom1 = TextField('surnom 1')
    surnom2 = TextField('surnom 2')
    surnom3 = TextField('surnom 3')
    ipq1 = TextField('IPQ 1')
    ipq2 = TextField('IPQ 2')
    ipq3 = TextField('IPQ 3')
    deviendra = TextField('Deviendra')
    souvenir = TextField('Mon meilleur souvenir avec')
    expression = TextField('Son expression favorite')
    imbattable = TextField('Est imbattable pour')
    dedicace = TextAreaField('Dédicace', render_kw={"rows": 3, "cols": 100})
    submit = SubmitField()


class ModDedicace(FlaskForm):
    surnom1 = TextField('surnom 1')
    surnom2 = TextField('surnom 2')
    surnom3 = TextField('surnom 3')
    ipq1 = TextField('IPQ 1')
    ipq2 = TextField('IPQ 2')
    ipq3 = TextField('IPQ 3')
    deviendra = TextField('Deviendra')
    souvenir = TextField('Mon meilleur souvenir avec')
    expression = TextField('Son expression favorite')
    imbattable = TextField('Est imbattable pour')
    dedicace = TextAreaField('Dédicace', render_kw={"rows": 3, "cols": 100})
    submit = SubmitField()


# Temporary fix for ValueError with SQLAlchemy 1.2 see https://github.com/wtforms/wtforms-sqlalchemy/issues/9
import wtforms_sqlalchemy.fields as f  # noqa


def get_pk_from_identity(obj):
    cls, key = f.identity_key(instance=obj)[:2]
    return ':'.join(f.text_type(x) for x in key)


f.get_pk_from_identity = get_pk_from_identity
