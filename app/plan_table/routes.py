from flask import render_template, redirect, request, url_for, flash, Blueprint, current_app
from flask_login import login_required, current_user
from .. import db
from ..models import User, DiplomeesSigma, PlanDeTable
from .forms import PrisedePlaces

bp = Blueprint('plan', __name__, template_folder='templates')


@bp.route('/plan-table', methods=['GET', 'POST'])
@login_required
def plan():
    plan = get_plan_to_show()
    for i,val in enumerate(plan): 
        plan[i].places_to_take = min(plan[i].nb_places_libre +1, 11-current_user.places_prises)
    return render_template("plan.html", title='Plan de table', tables=plan)


@bp.route('/add-table')
def add_table():
    user = current_user
    table = PlanDeTable()
    db.session.add(table)
    db.session.commit()
    flash('Table ajouté !')
    return redirect(url_for('plan.plan'))


def get_plan_to_show():
    plan_db = PlanDeTable.query.all()
    plan_to_show = []
    for table in plan_db:
        table_rendered = Table(table)
        plan_to_show.append(table_rendered)
    return plan_to_show

@bp.route('/addplace/', methods=['GET', 'POST'])
def addplace():
    tableid = request.form['tableid']
    userid = request.form['userid']
    nbplaces_str = request.form['nbPlace']
    table = PlanDeTable.query.filter_by(id=tableid).first()
    if not current_user.can_take_places():
        flash('La table n\'a pas assez de place !')
        return redirect(url_for('plan.plan'))
    else:
        user = current_user
        user.set_seats_taken(nbplaces_str)
        table.take_seats(userid, nbplaces_str)
        db.session.commit()
        flash('Places prises !')
        return redirect(url_for('plan.plan'))


@bp.route('/removeplace/', methods=['GET', 'POST'])
def removeplace():
    tableid = request.form['tableid']
    userid = request.form['userid']
    table = PlanDeTable.query.filter_by(id=tableid).first()
    user = current_user
    places_user = int(user.places_prises)
    places_removed = table.untake_seats(userid, places_user)
    user.set_seats_removed(places_removed)
    db.session.commit()
    return redirect(url_for('plan.plan'))


# To remove and refactor
class Table(object):
    # Utilisé uniquement pour l'affichage dans le tableau
    # Faire l'affichage dans une méthode dans models/plandetable
    id = 0
    nb_places_libre = 10
    user_is_in_table = False


    def __init__(self, table):
        self.id = table.id
        self.place1 = Table.is_none(self, table.place1)
        self.place2 = Table.is_none(self, table.place2)
        self.place3 = Table.is_none(self, table.place3)
        self.place4 = Table.is_none(self, table.place4)
        self.place5 = Table.is_none(self, table.place5)
        self.place6 = Table.is_none(self, table.place6)
        self.place7 = Table.is_none(self, table.place7)
        self.place8 = Table.is_none(self, table.place8)
        self.place9 = Table.is_none(self, table.place9)
        self.place10 = Table.is_none(self, table.place10)
        self.nb_places_libre = self.nb_places_libre
        self.user_is_in_table = self.is_user_in_table()
        self.count_places()

    def is_user_in_table(self):
        for place in [self.place1,self.place2,self.place3,self.place4,self.place5,
                        self.place6,self.place7,self.place8,self.place9,self.place10]:
            if not isinstance(place, str): 
                if place.email == current_user.email:
                    return True
        return False

    def count_places(self):
        nb_places_libre  = 0
        for place in [self.place1,self.place2,self.place3,self.place4,self.place5,
                        self.place6,self.place7,self.place8,self.place9,self.place10]:
            if place == 'Libre':
                nb_places_libre += 1
        self.nb_places_libre = nb_places_libre

    @staticmethod
    def is_none(self, place):
        if place is None:
            return 'Libre'
        else:
            return Table.get_name_of_place(place)

    @staticmethod
    def get_name_of_place(place):
        user = User.query.filter_by(id=place).first()
        return user
