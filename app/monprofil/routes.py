from flask import render_template, redirect, request, url_for, flash, Blueprint, current_app
from flask_login import login_required, current_user
from .. import db
from ..models import User
from .forms import EditProfile

bp = Blueprint('monprofil', __name__, template_folder='templates')


@bp.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
    form = EditProfile(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            user = current_user
            print(user)
            if form.prenom.data:
                user.prenom = form.prenom.data
            if form.nom.data:
                user.nom = form.nom.data
            if form.new_password.data:
                if user.check_password(form.old_password.data):
                    user.set_password(form.new_password.data)
                else:
                    flash('Ancien mot de passe non correspondant !')
                    return redirect(url_for('monprofil.edit'))
            db.session.add(user)
            db.session.commit()
            flash('Profil mis à jour !')
            return redirect(url_for('main.index'))
    return render_template('edit.html', form=form)
